# Try to retrieve version information as set by Jenking and extra information as retrieved
# from Git.
#
# Copyright (C) 2013 by Bart van der Velden <bart@muckingabout.eu>
# Copyright (C) 2011 by Johannes Wienke <jwienke at techfak dot uni-bielefeld dot de>
#
# This file may be licensed under the terms of the
# GNU Lesser General Public License Version 3 (the ``LGPL''),
# or (at your option) any later version.
#
# Software distributed under the License is distributed
# on an ``AS IS'' basis, WITHOUT WARRANTY OF ANY KIND, either
# express or implied. See the LGPL for the specific language
# governing rights and limitations.
#
# You should have received a copy of the LGPL along with this
# program. If not, go to http://www.gnu.org/licenses/lgpl.html
# or write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.


# Retrieves the short hash for the last commit: cbd1e87
# git log -1 --format=%h
function(git_last_version _hash)
    if(NOT GIT_FOUND)
        find_package(Git QUIET)
    endif()

    execute_process(COMMAND ${GIT_EXECUTABLE} log -1 --format=%h
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
        RESULT_VARIABLE GIT_RESULT
        OUTPUT_VARIABLE GIT_REV
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    if (${GIT_RESULT} EQUAL 0)
        set(${_hash} ${GIT_REV} PARENT_SCOPE)
    else()
        set(${_hash} "Git was not found" PARENT_SCOPE)
    endif()
endfunction()

# Retrieves a single line of information:
#
#   cbd1e67 Rename external to 3rdparty (#54)
# git log -1 --oneline
function(git_last_revision_single_line _info)
    if(NOT GIT_FOUND)
        find_package(Git QUIET)
    endif()

    execute_process(COMMAND ${GIT_EXECUTABLE} log -1 --oneline
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
        RESULT_VARIABLE GIT_RESULT
        OUTPUT_VARIABLE GIT_REV
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    if (${GIT_RESULT} EQUAL 0)
        set(${_info} ${GIT_REV} PARENT_SCOPE)
    else()
        set(${_info} "Git was not found" PARENT_SCOPE)
    endif()
endfunction()

# Retrieves the full information of the last commit:
#
#   commit cbd1e6712d127c07ef8bf85d5f82faf9b8c40c34
#   Author: Bart van der Velden <bart@muckingabout.eu>
#   Date:   Mon Dec 16 16:06:47 2013 +0100
#
#       Rename external to 3rdparty (#54)
#
# git log -1
function(git_last_revision_full_info _info)
    if(NOT GIT_FOUND)
        find_package(Git QUIET)
    endif()

    execute_process(COMMAND ${GIT_EXECUTABLE} log -1
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
        RESULT_VARIABLE GIT_RESULT
        OUTPUT_VARIABLE GIT_REV
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    if (${GIT_RESULT} EQUAL 0)
        set(${_info} ${GIT_REV} PARENT_SCOPE)
    else()
        set(${_info} "Git was not found" PARENT_SCOPE)
    endif()
endfunction()

