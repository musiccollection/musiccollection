# - If included before targets, sets compiler warning leves to a pedantic but
#   still useful level.
#
# This only works if included after the line containing the PROJECT call in
# CMake.
#
# Copyright (C) 2013 by Bart van der Velden <bart@muckingabout.eu>
# Copyright (C) 2011 by Johannes Wienke <jwienke at techfak dot uni-bielefeld dot de>
#
# This file may be licensed under the terms of the
# GNU Lesser General Public License Version 3 (the ``LGPL''),
# or (at your option) any later version.
#
# Software distributed under the License is distributed
# on an ``AS IS'' basis, WITHOUT WARRANTY OF ANY KIND, either
# express or implied. See the LGPL for the specific language
# governing rights and limitations.
#
# You should have received a copy of the LGPL along with this
# program. If not, go to http://www.gnu.org/licenses/lgpl.html
# or write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# The development of this software was supported by:
#   CoR-Lab, Research Institute for Cognition and Robotics
#     Bielefeld University
# A list of silly compiler warnings is maintained by Alf P. Steinbach at:
# http://alfps.wordpress.com/2010/06/05/cppx-w4-no-warnings-part-ii-disabling-msvc-sillywarnings/

include(CheckCXXCompilerFlag)

if(UNIX AND NOT APPLE) # On Linux

  if(CMAKE_COMPILER_IS_GNUCXX)
    CHECK_CXX_COMPILER_FLAG(-pipe CHECK_CXX_FLAG_pipe)
    if(CHECK_CXX_FLAG_pipe)
        add_definitions(-pipe)
    endif()
    CHECK_CXX_COMPILER_FLAG(-Wall CHECK_CXX_FLAG_Wall)
    if(CHECK_CXX_FLAG_Wall)
        add_definitions(-Wall)
    endif()
    CHECK_CXX_COMPILER_FLAG(-Wextra CHECK_CXX_FLAG_Wextra)
    if(CHECK_CXX_FLAG_Wextra)
        add_definitions(-Wextra)
    endif()
    CHECK_CXX_COMPILER_FLAG(-fdiagnostics-show-option CHECK_CXX_FLAG_DIAGNOSTICS)
    if(CHECK_CXX_FLAG_DIAGNOSTICS)
        add_definitions(-fdiagnostics-show-option)
    endif()
    # if you are insane, enable this...
    #CHECK_CXX_COMPILER_FLAG(-Weffc++ CHECK_CXX_FLAG_EFFCPP)
    #if(CHECK_CXX_FLAG_EFFCPP)
    #    add_definitions(-Weffc++)
    #endif()
  else()
    message(FATAL_ERROR "Unsupported C++ compiler on Linux")
  endif()

elseif(WIN32) # On Windows

  if(MSVC)
    add_definitions(/nologo)  # do not show the logo
    add_definitions(/W4)      # Always set to warning level 4 (with exceptions - see below)
    
    # do not warn for "unsafe" stl functions
    add_definitions(-D_CRT_NONSTDC_NO_DEPRECATE)
    add_definitions(-D_CRT_SECURE_NO_DEPRECATE)
    add_definitions(-D_SCL_SECURE_NO_DEPRECATE) 
  
    # Disable silly warnings
    add_definitions(/wd4127) # conditional expression is constant
    add_definitions(/wd4251) # needs to have dll-interface to be used by clients
    add_definitions(/wd4275) # exported class derived from non-exported class
    add_definitions(/wd4355) # "'this': used in member initializer list
    add_definitions(/wd4481) # nonstandard extension used: override specifier 'override'
    add_definitions(/wd4505) # unreferenced function has been removed
    add_definitions(/wd4510) # default constructor could not be generated
    add_definitions(/wd4512) # assignment operator could not be generated
    add_definitions(/wd4610) # can never be instantiated user defined constructor required

    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -DBOOST_FORCEINLINE=inline")
    
  else()
    message(FATAL_ERROR "Unsupported C++ compiler on Windows")
  endif()

endif()

#if(MSVC)
#
#    MESSAGE(STATUS "Disabling silly warnings for MSVC")
#
#    add_definitions(/wd4061) # enum value is not *explicitly* handled in switch
#    add_definitions(/wd4099) # first seen using 'struct' now seen using 'class'
#    add_definitions(/wd4217) # member template isn't copy constructor
#    add_definitions(/wd4250) # inherits (implements) some member via dominance
#    add_definitions(/wd4347) # "behavior change", function called instead of template
#    add_definitions(/wd4355) # "'this': used in member initializer list
#    add_definitions(/wd4511) # copy constructor could not be generated
#    add_definitions(/wd4513) # destructor could not be generated
#    add_definitions(/wd4623) # default constructor could not be generated
#    add_definitions(/wd4624) # destructor could not be generated
#    add_definitions(/wd4625) # copy constructor could not be generated
#    add_definitions(/wd4626) # assignment operator could not be generated
#    add_definitions(/wd4640) # a local static object is not thread-safe
#    add_definitions(/wd4661) # a member of the template class is not defined.
#    add_definitions(/wd4670) # a base class of an exception class is inaccessible for catch
#    add_definitions(/wd4672) # a base class of an exception class is ambiguous for catch
#    add_definitions(/wd4673) # a base class of an exception class is inaccessible for catch
#    add_definitions(/wd4675) # resolved overload was found by argument-dependent lookup
#    add_definitions(/wd4702) # unreachable code, e.g. in <list> header.
#    add_definitions(/wd4710) # call was not inlined
#    add_definitions(/wd4711) # call was inlined
#    add_definitions(/wd4820) # some padding was added
#    add_definitions(/wd4917) # a GUID can only be associated with a class, interface or namespace
#    add_definitions(/wd4619) # there is no warning number 'XXXX'
#    add_definitions(/wd4668) # XXX is not defined as a preprocessor macro
#
#endif()
