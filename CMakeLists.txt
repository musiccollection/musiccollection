# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2012-2014, Bart van der Velden <bart@muckingabout.eu>
# All rights reserved.
#
# ***** END GPL LICENSE BLOCK *****
#
# This CMakeLists.txt code is largely based on the following projects:
# * Blender (http://blender.org)
# * Amarok (http://amarok.kde.org)
# * CoR-Lab (https://code.cor-lab.org)
# 
# Thanks guys!

project(MusicCollection CXX)
cmake_minimum_required(VERSION 2.8.11)

#-----------------------------------------------------------------------------
# Set some options

# Set to On to have all CMakeLists files generate some extra output (-DCMAKE_DEGUG=On)
option(CMAKE_DEBUG "Generate extra CMake info" Off)

# When On build all tests. Turn off with -Dtest=off
option(test "Build all tests." On)

# We want to be able to run 'make test' to execute the unit tests
enable_testing()

#-----------------------------------------------------------------------------
# We don't allow in-source builds. This causes no end of troubles because
# all out-of-source builds will use the CMakeCache.txt file there and even
# build the libs and objects in it.

if(${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR})
    message(FATAL_ERROR
        "CMake generation for is not allowed within the source directory!"
        "\n Remove the CMakeCache.txt file and try again from another (sub)directory, e.g.:"
        "\n "
        "\n rm CMakeCache.txt"
        "\n mkdir build"
        "\n cd build"
        "\n cmake .."
    )
endif()

#-----------------------------------------------------------------------------
# Set some paths

# Set the path to the CMake modules
set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake/modules)
# Set output path
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/bin)
# Set library path
set(LIBRARY_OUTPUT_PATH ${PROJECT_BINARY_DIR}/lib)
# Set the path to the test data
set(TEST_DATA_PATH ${CMAKE_CURRENT_SOURCE_DIR}/data/testdata)

#-----------------------------------------------------------------------------
# Enable modules
include(EnableCoverageReport)     # Enable settings for coverage calculation
include(PedanticCompilerWarnings) # Global compiler flags

#-----------------------------------------------------------------------------
# Fix for compiling Google Mock and Test with Visual Studio 2012, see:
# stackoverflow.com/questions/8274588/c2977-stdtuple-too-many-template-arguments-msvc11
if (MSVC11)
    add_definitions(/D _VARIADIC_MAX=10)
endif(MSVC11)

#-----------------------------------------------------------------------------
# Third party libraries
add_subdirectory(3rdparty)

#-----------------------------------------------------------------------------
# Get version information
include(Version)

git_last_version(MUSICCOLLECTION_VERSION_GITSHA1)
git_last_revision_single_line(MUSICCOLLECTION_VERSION_GIT_LAST_REV_SINGLELINE)
git_last_revision_full_info(MUSICCOLLECTION_VERSION_GIT_LAST_REV_FULLINFO)

#-----------------------------------------------------------------------------
# Add the actual source code
add_subdirectory(src)

if(CMAKE_DEBUG)
    include(CMakeVariableLogging)
endif(CMAKE_DEBUG)
