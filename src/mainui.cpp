/*****************************************************************************
 * Copyright 2103 Bart van der Velden <bart@muckingabout.eu>                 *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/

#include "ui/mainwindow.h"
#include "version.h"

#include <QApplication>
#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    qDebug() << "Hello, world!";

    // Set the version information for later use
    Version version;
    QString ret = QString("Version %1.%2, Git revision: %3, Qt version: %4").
            arg(version.GetMajor(), version.GetMinor(), version.GetGitSha1(),
                qVersion());
    QCoreApplication::setApplicationVersion(ret);

    // Create and show the main window
    MainWindow w;
    w.show();

    return a.exec();
}
