/*****************************************************************************
 * Copyright 2014 Bart van der Velden <bart@muckingabout.eu>                 *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/

#ifndef MUSICCOLLECTION_COMMANDLINE_H_INCLUDED
#define MUSICCOLLECTION_COMMANDLINE_H_INCLUDED

#include <vector>
#include <boost/filesystem.hpp>

class CommandLine
{
public:
    explicit CommandLine();

    virtual ~CommandLine();

    virtual bool ParseCommandLine();

    virtual std::vector<boost::filesystem::path> GetDirectories() const;

    virtual boost::filesystem::path GetLogFile() const;

    virtual std::string GetErrorMessage() const;

    virtual std::string GetLogMessage() const;
private:

};

#endif // MUSICCOLLECTION_COMMANDLINE_H_INCLUDED
