/*****************************************************************************
 * Copyright 2014 Bart van der Velden <bart@muckingabout.eu>                 *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/

#ifndef MUSICCOLLECTION_HANDLECOMMANDLINE_H_INCLUDED
#define MUSICCOLLECTION_HANDLECOMMANDLINE_H_INCLUDED

#include <string>
#include <vector>
#include <boost/filesystem.hpp>

//! Parse the command line
/*! The options for this program can come from two sources: the command line and a (default)
    configuration file.

    --help, --h or -h : show the help. This option can only be combined with the version command
    Any other command or configuration will be ignored. This option is only available from the
    command line.

    --version, --v pr -v: show the version. This option can only be combined with the help command.
    Any other command or configuration will be ignored. This option is only available from the
    command line.

    --configfile: read options from the supplied configuration file. If this option is not given,
    an attempt is made to read from the file musiccollection.cfg in the current directory.
    
    Options
    found in the configuration file are merged with options given on the command line.

    \param argc the original argc as received through main()
    \param argv the original argv as received through main()
    \param directories the directories that were given as argument or read from the configuration
           file
    \param logFile the path to the logfile either as argument or read from the configuration file
    \param message an error message when the function returns false
    \return True if no errors occurred, otherwise false
 */
bool HandleCommandLine(int argc,
    char** argv,
    std::vector<boost::filesystem::path>& directories,
    boost::filesystem::path& logFile,
    std::string& message);

#endif // MUSICCOLLECTION_HANDLECOMMANDLINE_H_INCLUDED
