/*****************************************************************************
 * Copyright 2103 Bart van der Velden <bart@muckingabout.eu>                 *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                           *
 * This file is based on about.cpp as taken from the Clementine project      *
 * Copyright 2010, David Sansome <me@davidsansome.com>                       *
 * Clementine is free software: you can redistribute it and/or modify        *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *****************************************************************************/

#include "about.h"
#include "ui_about.h"

#include <QPushButton>

About::About(QWidget *parent)
    : QDialog(parent)
{
    m_ui.setupUi(this);

    setWindowTitle(tr("About %1").arg(QCoreApplication::applicationName()));
    m_ui.title->setText(QCoreApplication::applicationName());
    m_ui.version->setText(tr("Version %1").arg(QCoreApplication::applicationVersion()));

    QFont title_font;
    title_font.setBold(true);
    title_font.setPointSize(title_font.pointSize() + 4);
    m_ui.title->setFont(title_font);

    m_urls
        << Contributor("Gitorious repository", 0, "https://gitorious.org/musiccollection")
        << Contributor("Jenkins Dashboard", 0,
                       "https://jenkins.muckingabout.eu/job/BuildMusicCollection/")
        << Contributor("Redmine Project Management", 0,
                       "https://redmine.muckingabout.eu/projects/music_collection");

    m_authors << Contributor(QString::fromUtf8("Bart van der Velden"), "bart@muckingabout.eu", 0);
    m_thanksTo
        << Contributor("The Clementine Music Player Project", 0,
                       "http://www.clementine-player.org/")
        << Contributor("The Amarok Project", 0, "http://amarok.kde.org/");

    qSort(m_authors);
    qSort(m_thanksTo);

    m_ui.content->setHtml(MakeHtml());

    m_ui.buttonBox->button(QDialogButtonBox::Close)->setShortcut(QKeySequence::Close);
}

QString About::MakeHtml() const
{
    QString ret = QString("<p><b>%3:</b>").arg(tr("Project URLs"));

    foreach(const Contributor& contributor, m_urls)
    {
        ret += "<br />" + MakeHtml(contributor);
    }

    ret += QString("</p><p><b>%3:</b>").arg(tr("Authors"));
    foreach(const Contributor& contributor, m_authors)
    {
        ret += "<br />" + MakeHtml(contributor);
    }

    ret += QString("</p><p><b>%3:</b>").arg(tr("Thanks to"));

    foreach (const Contributor& contributor, m_thanksTo)
    ret += "<br />" + MakeHtml(contributor);

    return ret;
}

QString About::MakeHtml(const Contributor& contributor) const
{
    const QString mailTemplate = " &lt;<a href=\"mailto:%1\">%2</a>&gt;";
    const QString urlTemplate = " &lt;<a href=\"%1\">%2</a>&gt;";
    QString retVal = contributor.m_name;
    if (!contributor.m_email.isNull())
    {
        retVal += QString(mailTemplate).arg(contributor.m_email, contributor.m_email);
    }
    if (!contributor.m_url.isNull())
    {
        retVal += QString(urlTemplate).arg(contributor.m_url, contributor.m_url);
    }
    return retVal;
}
