/*****************************************************************************
 * Copyright 2103 Bart van der Velden <bart@muckingabout.eu>                 *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "ui/about.h"
#include <QFileDialog>
#include "FileTypeResolver.h"
#include "taglib.h"

//! Set-up the main window
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_ui(new Ui::MainWindow)
{
    m_ui->setupUi(this);

    // Connect signals and slots
    connect(m_ui->browseButton, SIGNAL(clicked()), SLOT(BrowseForFile()));
    connect(m_ui->lookupButton, &QPushButton::clicked, this, &MainWindow::LookupFile);
    connect(m_ui->action_about_musiccollection, SIGNAL(triggered()), SLOT(ShowAboutDialog()));
    connect(m_ui->action_about_qt, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
    connect(m_ui->actionExit, SIGNAL(triggered()), qApp, SLOT(quit()));

    // Set icons
    m_ui->action_about_musiccollection->setIcon(QIcon(":/icon.png"));
    m_ui->action_about_qt->setIcon(QIcon(":/qt-project.org/qmessagebox/images/qtlogo-64.png"));
}

//! Clean-up the dialogue
MainWindow::~MainWindow()
{
    delete m_ui;
}

//! Start a file open dialogue to select a file
void MainWindow::BrowseForFile()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Select File"));

    if (!filename.isEmpty())
    {
        m_ui->filenameLineEdit->setText(filename);
    }
}

void MainWindow::LookupFile()
{
    mc::collection::FileTypeResolver ftr;
    ftr.createFile(m_ui->filenameLineEdit->text().toStdString().c_str());
}

//! Show the about box
void MainWindow::ShowAboutDialog()
{
    if (!m_aboutDialog)
    {
      m_aboutDialog.reset(new About);
    }

    m_aboutDialog->setModal(true);
    m_aboutDialog->show();
}
