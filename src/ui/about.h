/*****************************************************************************
 * Copyright 2103 Bart van der Velden <bart@muckingabout.eu>                 *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                           *
 * This file is based on about.h as taken from the Clementine project        *
 * Copyright 2010, David Sansome <me@davidsansome.com>                       *
 * Clementine is free software: you can redistribute it and/or modify        *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *****************************************************************************/

#ifndef MUSICCOLLECTION_UI_ABOUT_H_INCLUDED
#define MUSICCOLLECTION_UI_ABOUT_H_INCLUDED

#include <QDialog>
#include "ui_about.h"

class About : public QDialog
{
    Q_OBJECT
public:
    About(QWidget* parent = 0);

    struct Contributor
    {
        Contributor(const QString& name, const QString& email, const QString& url)
            : m_name(name), m_email(email), m_url(url) {}

        bool operator <(const Contributor& other) const { return m_name < other.m_name; }

        QString m_name;
        QString m_email;
        QString m_url;
    };

private:
    QString MakeHtml() const;
    QString MakeHtml(const Contributor& contributor) const;

private:
    Ui::About m_ui;

    QList<Contributor> m_urls; // The project urls
    QList<Contributor> m_authors; // The list of authors
    QList<Contributor> m_thanksTo; // The list of people/projects to thank
};

#endif // MUSICCOLLECTION_UI_ABOUT_H_INCLUDED
