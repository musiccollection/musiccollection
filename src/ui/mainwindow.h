/*****************************************************************************
 * Copyright 2103 Bart van der Velden <bart@muckingabout.eu>                 *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <boost/scoped_ptr.hpp>
#include <QMainWindow>

namespace Ui
{
    // Forward declaration for Ui::MainWindow
    class MainWindow;
}

// Forward declaration for the About dialogue
class About;

//! MainWindow is the start dialogue of the application
class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    //! Set-up the main window
    explicit MainWindow(QWidget *parent = 0);

    //! Clean-up the dialogue
    ~MainWindow();

private slots:
    //! Start a file open dialogue to select a file
    void BrowseForFile();

    void LookupFile();

    //! Show the about box
    void ShowAboutDialog();

private:
    //! The MainWindow UI class
    Ui::MainWindow* m_ui;

    //! The About dialogue is encapsulated in a scoped_ptr for resource management
    boost::scoped_ptr<About> m_aboutDialog;
};

#endif // MAINWINDOW_H
