/*****************************************************************************
 * Copyright 2012-2103 Bart van der Velden <bart@muckingabout.eu>            *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/

#include <iostream>
#include <boost/chrono.hpp>
#include <boost/locale.hpp>

// Boost log
#include "boost_log_trivial.h"
#include <boost/log/core.hpp>
#include <boost/log/sinks.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/detail/utf8_codecvt_facet.hpp>

#include "HandleCommandLine.h"
#include "scandir.h"
#include "utils.h"
#include <boost/algorithm/string.hpp>
#include <taglib.h>
#include <tag.h>
#include <tpropertymap.h>
#include <fileref.h>

using namespace std;
namespace fs = boost::filesystem;
namespace po = boost::program_options;
namespace chrono = boost::chrono;
namespace utils = mc::utils;
namespace conv = boost::locale::conv;
namespace logging = boost::log;
namespace keywords = boost::log::keywords;
namespace sinks = boost::log::sinks;

static const int Second = 1000;

//! Setup logging to the given logFile
/*!
 */
void InitLogging(const fs::path& logFile)
{
    //fs::path ext = logFile.
    fs::path logFilePattern(logFile);
    logFilePattern += ".%N.log";
    boost::shared_ptr<sinks::synchronous_sink<sinks::text_file_backend > > sink = logging::add_file_log
    (
        keywords::file_name = logFilePattern,
        keywords::open_mode = ios_base::out,
        keywords::rotation_size = 10 * 1024 * 1024,
        keywords::time_based_rotation = boost::log::sinks::file::rotation_at_time_point(0, 0, 0),
        keywords::format = "[%TimeStamp%]: %Message%"
    );

    logging::core::get()->set_filter
    (
        logging::trivial::severity >= logging::trivial::info
    );
    std::locale loc = boost::locale::generator()("en_US.UTF-8");
    sink->imbue(loc);

    logging::add_common_attributes();
}

// Return true if at least one of the scanners is not yet finished
bool Scanning(const vector<utils::ScanDirPtr>& dirScans)
{
    auto scanning = false;
    for (auto it = dirScans.begin(); !scanning && it != dirScans.end(); ++it)
    {
        scanning = !(*it)->ScanFinished();
    }
    return scanning;
}

class AudioFile
{
public:
    AudioFile(const TagLib::FileRef& fileRef);

//private:
    std::string m_artistName;
    std::string m_trackTitle;
    std::string m_albumTitle;
    std::string m_date;
    std::string m_albumArtist;
    std::string m_fileName;
    std::string m_directoryName;

    size_t m_fileSize;
    unsigned int m_numSamples;
    unsigned int m_sampleRate;
    unsigned int m_channels;
    unsigned int m_bitrate;
};

AudioFile::AudioFile(const TagLib::FileRef& fileRef)
    : m_numSamples(0),
      m_channels(0),
      m_bitrate(0)
{
    m_artistName = conv::utf_to_utf<char>(fileRef.tag()->artist().toWString());
    m_trackTitle = conv::utf_to_utf<char>(fileRef.tag()->title().toWString());
    m_albumTitle = conv::utf_to_utf<char>(fileRef.tag()->album().toWString());

    m_sampleRate = fileRef.audioProperties()->sampleRate();
    TagLib::StringList sl = fileRef.file()->properties().unsupportedData();
    size_t len = fileRef.file()->length();
    m_fileSize = len;
    TagLib::PropertyMap pm = fileRef.file()->properties();
    TagLib::String s = pm.toString();
    if (s.length() > 0)
    {
        BOOST_LOG_TRIVIAL(info) << "Properties:\n" << s.toCString(true);
    }
}

//! Handle a file with a .mp3 extension
/*! At the moment each file that is retrieved while scanning the directory is handed to
    HandleMp3.
 */
class HandleMp3 : public utils::IProcessFile
{
public:
    HandleMp3()
    {
    }

    bool Process(const boost::filesystem::path& Path)
    {
        // Only look for files with the .mp3 extension
        if (boost::iequals(Path.extension().native(), L".mp3")||
            boost::iequals(Path.extension().native(), L".ogg"))
        {
            TagLib::FileRef f(Path.native().c_str());
            AudioFile file(f);

            return true;
        }
        else
        {
            return false;
        }
    }
};

int ConnnectToDatabase()
{
    return 0;
}

int main(int argc, char **argv)
{
    int retVal = 0;
    vector<fs::path> directories;
    fs::path logFile;
    string errMsg;
    if (HandleCommandLine(argc, argv, directories, logFile, errMsg))
    {
        InitLogging(logFile);

        using namespace logging::trivial;
        BOOST_LOG_TRIVIAL(info) << "Starting MusicCollection";

        // Start timing
        chrono::system_clock::time_point start = chrono::system_clock::now();

        ConnnectToDatabase();
        HandleMp3 process;
        vector<utils::ScanDirPtr> dirScans;
        // Start a scanner for each given directory
        for (auto it = directories.begin(); it != directories.end(); ++it)
        {
            BOOST_LOG_TRIVIAL(info) << "Scanning " << *it;

            // Prepare for long file names on Windows
            fs::path path = utils::PrepareLongFileName(*it);
            dirScans.push_back(utils::ScanDirPtr(new utils::ScanDir(path, &process)));
        }

        // While waiting for all scanners to finish their work, print a dot each second to
        // show that we are still working
        while (Scanning(dirScans))
        {
            boost::this_thread::sleep(boost::posix_time::milliseconds(Second));
            cout << ".";
            cout.flush();
        }
        cout << endl;

        // Print summaries
        for (auto it = dirScans.begin(); it != dirScans.end(); ++it)
        {
            cout << "Summary for "                   << (*it)->StartDir() << "\n";
            cout << "Total number of directories : " << (*it)->NumDirs()  << "\n";
            cout << "Total number of files       : " << (*it)->NumFiles() << "\n";
            string totalSize = mc::utils::FormatSize((*it)->TotalSize(), 0);
            cout << "Total size of all files     : " << totalSize         << "\n\n";
        }

        // Show spent time
        chrono::duration<double> sec = chrono::system_clock::now() - start;
        std::cout << "Scanning took " << sec.count() << " seconds\n";

        BOOST_LOG_TRIVIAL(info) << "Stopping MusicCollection";
    }
    else
    {
        // Write errMsg to the console
        cout << errMsg << "\n";
        retVal = 1;
    }

    return retVal;
}
