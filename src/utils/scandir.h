/*****************************************************************************
 * Copyright 2013 Bart van der Velden <bart@muckingabout.eu>                 *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/

/*!
 * \brief Recursively traverse a directory and store all file info
 */

#ifndef MUSICCOLLECTION_UTILS_SCANDIR_H_INCLUDED
#define MUSICCOLLECTION_UTILS_SCANDIR_H_INCLUDED

#include "boost_thread.h"
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

namespace mc {
    namespace utils {

        //! Interface to handle a given file
        /*! This interface is used to process each individual file that is
            scanned by ScanDir.
         */
        class IProcessFile
        {
        public:
            //! Process the given file.
            /*! Return true if the file was processed, false when it was
                ignored.
             */
            virtual bool Process(const boost::filesystem::path& Path) = 0;
        };

        //! Recursively traverse a directory and store all file info.
        /*! ScanDir class recursively walks the path that is given in its
            constructor. This happens in a separate thread since this action
            can take quite a bit of time.
          */
        class ScanDir
        {
        public:
            //! Start scanning Path recursively in a new thread.
            /*! Feed every found file name to the give fileProcess
             */
            ScanDir(const boost::filesystem::path& Path, IProcessFile* FileProcess);

            //! Stops scanning and joins the thread
            ~ScanDir();

            //! Returns true if scanning the directory was finished
            /*! If scanning is still in progress ScanFinished returns false.
             */
            bool ScanFinished() const;

            //! Return the directory where scanning started.
            /*! This is the directory that was passed to the constructor.
             */
            boost::filesystem::path StartDir() const;

            //! Return the directory that is currently being scanned.
            /*! Or, if scanning has already finished, the last directory that
                was scanned.
             */
            boost::filesystem::path CurrentDir() const;

            //! Return the number of directories found during scanning so far.
            int NumDirs() const;

            //! Return the number of files found during scanning so far.
            int NumFiles() const;

            //! Return the total size of all files found so far.
            uintmax_t TotalSize() const;

            void List();
        private:
            //! If set to true the scanning will be aborted shortly
            volatile bool m_stopRequested;
            //! True if scanning was finished
            bool m_finished;
            //! The IFileProcess to defer each file to
            IProcessFile* m_fileProcess;
            //! The directory where scanning started
            boost::filesystem::path m_startDir;
            //! The current or last directory that was scanned
            boost::filesystem::path m_currentDir;
            //! The number of files found so far
            int m_numFiles;
            //! The number of directories found so far
            int m_numDirs;
            //! The total size of all files found so far
            uintmax_t m_totalSize;

            boost::thread m_thread;

            //! Do the actual scanning
            void DoWork();

            //! Not implemented
            ScanDir();
            ScanDir(const ScanDir&);
            ScanDir& operator=(const ScanDir&);
        };

        typedef boost::shared_ptr<ScanDir> ScanDirPtr;

    } // namespace utils
} // namespace mc

#endif // MUSICCOLLECTION_UTILS_SCANDIR_H_INCLUDED
