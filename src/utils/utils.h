/*****************************************************************************
 * Copyright 2012-2013 Bart van der Velden <bart@muckingabout.eu>            *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/

#ifndef MUSICCOLLECTION_UTILS_UTILS_H_INCLUDED
#define MUSICCOLLECTION_UTILS_UTILS_H_INCLUDED

#include <string>
#include <boost/cstdint.hpp>
#include <boost/filesystem.hpp>

namespace mc
{
    namespace utils
    {
        //! Format Size 
        std::string FormatSize(uintmax_t Size, uintmax_t MaxSize);

        //! Prepare for long file names on Windows
        /*! On Windows MAX_PATH remains at 260 no matter what. To trick the 
            WinApi functions into handling file names that are longer than
            MAX_PATH, we need to put the magic "\\?" before the drive letter.
         */
        boost::filesystem::path PrepareLongFileName(const boost::filesystem::path& Path);

    } // namespace utils
} // namespace mc

#endif // MUSICCOLLECTION_UTILS_UTILS_H_INCLUDED
