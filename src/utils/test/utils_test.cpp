#include <gtest/gtest.h>
#include "../utils.h"

using namespace std;
using namespace mc::utils;
namespace fs = boost::filesystem;

static const uintmax_t Kilo = 1024;
static const uintmax_t Mega = Kilo * Kilo;
static const uintmax_t Giga = Kilo * Mega;
static const uintmax_t Tera = Kilo * Giga;

TEST(UtilsTest, PrepareLongFileNameWithDrive)
{
    fs::path res = PrepareLongFileName("c:\\autoexec.bat");
#if defined(_WIN32)
    EXPECT_STREQ(L"\\\\?\\c:\\autoexec.bat", res.c_str());
#else
    EXPECT_STREQ("c:\\autoexec.bat", res.c_str());
#endif
}

TEST(UtilsTest, PrepareLongFileNameNoDrive1)
{
    fs::path res = PrepareLongFileName("relative\\path");
#if defined(_WIN32)
    EXPECT_STREQ(L"relative\\path", res.c_str());
#else
    EXPECT_STREQ("relative\\path", res.c_str());
#endif
}

TEST(UtilsTest, PrepareLongFileNameNoDrive2)
{
    fs::path res = PrepareLongFileName("a");
#if defined(_WIN32)
    EXPECT_STREQ(L"a", res.c_str());
#else
    EXPECT_STREQ("a", res.c_str());
#endif
}

TEST(UtilsTest, PrepareLongFileNameNoDrive3)
{
    fs::path res = PrepareLongFileName("a:b");
#if defined(_WIN32)
    EXPECT_STREQ(L"a:b", res.c_str());
#else
    EXPECT_STREQ("a:b", res.c_str());
#endif
}

TEST(UtilsTest, PrepareLongFileNameNonAnsiCharacters)
{
    fs::path res = PrepareLongFileName("c:\\autoexec_テスト.bat");
#if defined(_WIN32)
    EXPECT_STREQ(L"\\\\?\\c:\\autoexec_テスト.bat", res.c_str());
#else
    EXPECT_STREQ("c:\\autoexec_テスト.bat", res.c_str());
#endif
}

// Tests with the second parameter 0
TEST(UtilsTest, FormatSizeSecondParameterZero)
{
    string res = FormatSize(Kilo, 0);
    EXPECT_STREQ("1.0 KiB", res.c_str());

    res = FormatSize(0, 0);
    EXPECT_STREQ("0 byte", res.c_str());

    res = FormatSize(1023, 0);
    EXPECT_STREQ("1023 byte", res.c_str());

    res = FormatSize(10, 0);
    EXPECT_STREQ("10 byte", res.c_str());

    res = FormatSize(Mega, 0);
    EXPECT_STREQ("1.0 MiB", res.c_str());

    res = FormatSize(2 * Mega + (100 * Kilo), 0);
    EXPECT_STREQ("2.1 MiB", res.c_str());

    res = FormatSize(Giga, 0);
    EXPECT_STREQ("1.0 GiB", res.c_str());

    res = FormatSize(Giga + (100 * Mega), 0);
    EXPECT_STREQ("1.1 GiB", res.c_str());

    res = FormatSize(Giga + (200 * Mega), 0);
    EXPECT_STREQ("1.2 GiB", res.c_str());

    res = FormatSize(Tera, 0);
    EXPECT_STREQ("1.0 TiB", res.c_str());

    res = FormatSize(Kilo * Tera, 0);
    EXPECT_STREQ("1024.0 TiB", res.c_str());

    res = FormatSize(Mega * Tera, 0);
    EXPECT_STREQ("1048576.0 TiB", res.c_str());

}

// Tests with a second parameter in the same range as the given size to display
TEST(UtilsTest, FormatSizeSameRangeSecondParameter)
{
    string res = FormatSize(0, Kilo);
    EXPECT_STREQ("0.0 KiB", res.c_str());

    res = FormatSize(Kilo - 100, Kilo);
    EXPECT_STREQ("0.9 KiB", res.c_str());

    res = FormatSize(0, Mega);
    EXPECT_STREQ("0.0 MiB", res.c_str());

    res = FormatSize(Mega - (100 * Kilo), Mega);
    EXPECT_STREQ("0.9 MiB", res.c_str());

    res = FormatSize(0, Giga);
    EXPECT_STREQ("0.0 GiB", res.c_str());

    res = FormatSize(Giga - (100 * Mega), Giga);
    EXPECT_STREQ("0.9 GiB", res.c_str());

    res = FormatSize(0, Tera);
    EXPECT_STREQ("0.0 TiB", res.c_str());

    res = FormatSize(Tera - (100 * Giga), Tera);
    EXPECT_STREQ("0.9 TiB", res.c_str());
}

// Tests with a second parameter larger than the given size to display
TEST(UtilsTest, FormatSizeSmallerSecondParameter)
{
    string res = FormatSize(Mega, Kilo);
    EXPECT_STREQ("1024.0 KiB", res.c_str());

    res = FormatSize(Giga, Kilo);
    EXPECT_STREQ("1048576.0 KiB", res.c_str());

    res = FormatSize(Tera, Kilo);
    EXPECT_STREQ("1073741824.0 KiB", res.c_str());

    res = FormatSize(Giga, Mega);
    EXPECT_STREQ("1024.0 MiB", res.c_str());

    res = FormatSize(Tera, Mega);
    EXPECT_STREQ("1048576.0 MiB", res.c_str());

    res = FormatSize(Tera, Giga);
    EXPECT_STREQ("1024.0 GiB", res.c_str());
}
