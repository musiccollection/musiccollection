/*****************************************************************************
 * Copyright 2013 Bart van der Velden <bart@muckingabout.eu>                 *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/

#include "utils.h"
#include <boost/format.hpp>

using namespace std;
using boost::format;
namespace fs = boost::filesystem;

namespace mc
{
    namespace utils {

        //! Prepare for long file names on Windows
        /*! On Windows MAX_PATH remains at 260 no matter what. To trick the
            WinApi functions into handling file names that are longer than
            MAX_PATH, we need to put the magic "\\?" before the drive letter.

            Note: this only works for absolute paths
         */
        fs::path PrepareLongFileName(const fs::path& Path)
        {
#if defined(_WIN32)
            fs::path retval(Path);
            wstring rootPath = retval.root_path().native();
            if (rootPath.size() > 2 &&
                rootPath[1] == ':' &&
                (rootPath[2] == '\\' || rootPath[2] == '/'))
            {
                retval = L"\\\\?\\" + retval.native();
            }
            return retval;
#else
            return Path;
#endif
        }

        string FormatSize(uintmax_t Size, uintmax_t MaxSize)
        {
            enum
            {
                k_index,
                m_index,
                g_index,
                t_index
            };

            struct Format
            {
                uintmax_t factor;
                const char* format;
            };

            const Format allFormats[4] =
            {
                { 1ULL << 10, "%.1f KiB" },
                { 1ULL << 20, "%.1f MiB" },
                { 1ULL << 30, "%.1f GiB" },
                { 1ULL << 40, "%.1f TiB" }
            };

            string retVal;

            if (MaxSize == 0)
            {
                MaxSize = Size;
            }

            if (MaxSize < allFormats[k_index].factor)
            {
                retVal = str(boost::format("%u byte") % Size);
            }
            else
            {
                uintmax_t factor;
                const char* format = 0;

                if (MaxSize < allFormats[m_index].factor)
                {
                    factor = allFormats[k_index].factor;
                    format = allFormats[k_index].format;
                }
                else if (MaxSize < allFormats[g_index].factor)
                {
                    factor = allFormats[m_index].factor;
                    format = allFormats[m_index].format;
                }
                else if (MaxSize < allFormats[t_index].factor)
                {
                    factor = allFormats[g_index].factor;
                    format = allFormats[g_index].format;
                }
                else
                {
                    factor = allFormats[t_index].factor;
                    format = allFormats[t_index].format;
                }

                retVal = str(boost::format(format) %
                    (static_cast<double> (Size) / static_cast<double> (factor)));
            }

            return retVal;
        }
    }
}
