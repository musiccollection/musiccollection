/*****************************************************************************
 * Copyright 2013 Bart van der Velden <bart@muckingabout.eu>                 *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/

///////////////////////////////////////////////////////////////////////////////
// ScanDir class recursively walks the path that is given in its constructor.
// This happens in a separate thread since this action can take quite a bit
// of time.

#include "scandir.h"
#include <string>
#include <iostream>
#include "boost_log_trivial.h"

using namespace std;
namespace fs = boost::filesystem;
using namespace mc::utils;
namespace logging = boost::log;

// Start scanning Path recursively in a new thread
ScanDir::ScanDir(const fs::path& Path, IProcessFile* FileProcess)
    : m_stopRequested(false),
      m_finished(false),
      m_fileProcess(FileProcess),
      m_startDir(Path),
      m_currentDir(Path),
      m_numFiles(0),
      m_numDirs(0),
      m_totalSize(0),
      m_thread(boost::thread(std::bind(&ScanDir::DoWork, this)))
{
}

// Stops scanning and joins the thread
ScanDir::~ScanDir()
{
    m_stopRequested = true;
    m_thread.join();
}

// Returns true if scanning the directory was finished, otherwise false
bool ScanDir::ScanFinished() const
{
    return m_finished;
}

// Return the directory where scanning started. This is the directory that was
// passed to the constructor.
fs::path ScanDir::StartDir() const
{
    return m_startDir;
}

// Return the directory that is currently being scanned. Or, if scanning has
// already finished, the last directory that was scanned.
fs::path ScanDir::CurrentDir() const
{
    return m_currentDir;
}

// Return the number of directories found during scanning so far.
int ScanDir::NumDirs() const
{
    return m_numDirs;
}

// Return the number of files found during scanning so far.
int ScanDir::NumFiles() const
{
    return m_numFiles;
}

// Return the total size of all files found so far.
uintmax_t ScanDir::TotalSize() const
{
    return m_totalSize;
}

// Do the actual scanning
void ScanDir::DoWork()
{
    boost::system::error_code ec;
    fs::recursive_directory_iterator it(m_currentDir, ec);
    fs::recursive_directory_iterator end;
    if (ec)
    {
        BOOST_LOG_TRIVIAL(error) << "\nError! Errorcode = " << ec.value() << ": " << ec.message() << " (" << m_currentDir.string();
    }

    while (it != end && !m_stopRequested)
    {
        boost::system::error_code ec;
        bool ex = fs::exists(*it, ec);
        if (ex)
        {
            if (fs::is_directory(*it))
            {
                m_currentDir = *it;
                m_numDirs++;
                if (fs::is_symlink(*it))
                {
                    it.no_push();
                }
            }
            if (fs::is_regular_file(*it))
            {
                // If we have supplied a process, run it on the given file.
                if (m_fileProcess)
                {
                    if (m_fileProcess->Process(*it))
                    {
                        // If the process returns true, indicating that it
                        // handled the file, add it to the statistics.
                        m_numFiles++;
                        m_totalSize += file_size(*it);
                    }
                }
                else
                {
                    // We have no process, add every file.
                    m_numFiles++;
                    m_totalSize += file_size(*it);
                }
            }
        }
        try
        {
            ++it;
        }
        catch (fs::filesystem_error& err)
        {
            BOOST_LOG_TRIVIAL(error) << err.what();
            it.no_push();
        }
    }

    m_finished = true;
    using namespace logging::trivial;
    BOOST_LOG_TRIVIAL(info) << "Finished scanning " << m_startDir;

    m_stopRequested = true;
}
