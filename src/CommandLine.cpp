/*****************************************************************************
 * Copyright 2014 Bart van der Velden <bart@muckingabout.eu>                 *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/

#include "CommandLine.h"
#include <fstream>
#include <boost/program_options.hpp>
#include <boost/exception/all.hpp>
#include <boost/exception/get_error_info.hpp>
#include "version.h"

CommandLine::CommandLine()
{

}

CommandLine::~CommandLine()
{

}

bool CommandLine::ParseCommandLine()
{
    return false;
}

std::vector<boost::filesystem::path> CommandLine::GetDirectories() const
{
    std::vector<boost::filesystem::path> directories;
    return directories;
}

boost::filesystem::path CommandLine::GetLogFile() const
{
    boost::filesystem::path logFile;
    return logFile;
}

std::string CommandLine::GetErrorMessage() const
{
    std::string retVal;
    return retVal;
}

std::string CommandLine::GetLogMessage() const
{
    std::string retVal;
    return retVal;
}
