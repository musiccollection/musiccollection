/*****************************************************************************
 * Copyright 2014 Bart van der Velden <bart@muckingabout.eu>                 *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/

#include "HandleCommandLine.h"
#include <fstream>
#include <boost/program_options.hpp>
#include <boost/exception/all.hpp>
#include <boost/exception/get_error_info.hpp>
#include "version.h"

using namespace std;
namespace fs = boost::filesystem;
namespace po = boost::program_options;

static const char* DefaultLogFile = "mc";
static const char* DefaultConfigFile = "musiccollection.cfg";

void AddErrorMessage(string& orgMessage, const string& newMessage)
{
    if (orgMessage.length() == 0)
    {
        orgMessage = newMessage;
    }
    else
    {
        orgMessage += "\n";
        orgMessage += newMessage;
    }
}

//! Parse the command line
/*! The options for this program can come from two sources: the command line and a (default)
    configuration file.

    --help, --h or -h : show the help. This option can only be combined with the version command
    Any other command or configuration will be ignored. This option is only available from the
    command line.

    --version, --v pr -v: show the version. This option can only be combined with the help command.
    Any other command or configuration will be ignored. This option is only available from the
    command line.

    --configfile: read options from the supplied configuration file. If this option is not given,
    an attempt is made to read from the file musiccollection.cfg in the current directory.
    
    Options
    found in the configuration file are merged with options given on the command line.

    
 */
bool HandleCommandLine(
    int argc,
    char **argv,
    vector<fs::path>& directories,
    fs::path& logFile,
    string& message)
{
    auto retVal = true;
    try
    {
        po::positional_options_description pod;
        pod.add("directory", -1);

        po::variables_map vm;
        fs::path configFile(DefaultConfigFile);

        // Generic options: help and version. These can only be used from the command line
        po::options_description genericOptions("Generic options");
        genericOptions.add_options()
            ("help,h", "show this help")
            ("version,v", "show version information")
            ("configfile",
                po::value<fs::path>(&configFile)->default_value(DefaultConfigFile),
                    "the configuration file to read for other options. musiccollection.cfg is used "
                    "if no configuration file name is supplied");

        // Configuration options: logfile and directories. These can be used both from
        // the command line and a configuration file. Only one logfile is used. If a logfile is
        // specified on the command line and in the configuration file, the command line option
        // takes precedence. Multiple directory values from the command line and the configuration
        // file are merged.
        po::options_description configOptions("Configuration");
        configOptions.add_options()
            ("logfile",
                po::value<fs::path>(),
                "the file to log to")
            ("directory",
                po::value<vector<fs::path> >()->composing(),
                "directory to scan. A directory can also be given without the --directory.");

        po::options_description cmdLineOptions;
        cmdLineOptions.add(genericOptions).add(configOptions);
        try
        {
            po::store(po::command_line_parser(argc, argv).options(cmdLineOptions).positional(pod).run(), vm);
            ifstream ifs(configFile.native());
            po::store(po::parse_config_file(ifs, configOptions), vm);
            po::notify(vm);
        }
        catch (boost::program_options::invalid_syntax& e)
        {
            stringstream ss;
            ss << "Caught an invalid_syntax exception: " << e.what();
            AddErrorMessage(message, ss.str());
            retVal = false;
        }

        if (vm.count("help"))
        {
            // Help is the only action required.
            stringstream ss;
            ss << cmdLineOptions;
            AddErrorMessage(message, ss.str());
            retVal = false;
        }

        if (vm.count("version"))
        {
            // version may be asked at the same time as help, but it doesn't have to be
            // The program will not continue after asking it for its version information
            Version version;
            stringstream ss;
            ss << "MusicCollection, version: " << version.GetMajor() << "." <<
                version.GetMinor() << ", git revision: " << version.GetGitSha1() << "\n";
            ss << version.Copyright() << "\n";
            AddErrorMessage(message, ss.str());
            retVal = false;
        }

        if (retVal == true)
        {
            if (vm.count("directory") == 0)
            {
                stringstream ss;
                ss << "Need to supply at least one directory\n";
                ss << cmdLineOptions << "\n";
                AddErrorMessage(message, ss.str());
                retVal = false;
            }
            else
            {
                stringstream ss;
                ss << "Directories are: \n";
                directories    = vm["directory"].as< vector<fs::path> >();
                for (vector<fs::path>::iterator it = directories   .begin(); it != directories   .end(); ++it)
                {
                    ss  << "\t" << *it << "\n";
                }
                AddErrorMessage(message, ss.str());
            }
            if (vm.count("logfile") == 1)
            {
                logFile = vm["logfile"].as<fs::path> ();
            }
            else
            {
                logFile = DefaultLogFile;
            }
        }
    }
    catch(po::unknown_option& option)
    {
        stringstream ss;
        ss << "Unknown option: " << option.get_option_name() << "\n";
        AddErrorMessage(message, ss.str());
        retVal = false;
    }
    catch(boost::exception&)
    {
        stringstream ss;
        ss << "Unhandled exception: " << "\n";
        ss << boost::current_exception_diagnostic_information() << "\n";
        AddErrorMessage(message, ss.str());
        retVal = false;
    }
    return retVal;
}
