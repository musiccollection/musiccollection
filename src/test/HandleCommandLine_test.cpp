#include <gtest/gtest.h>
#include "../HandleCommandLine.h"

using namespace std;
namespace fs = boost::filesystem;

TEST(HandleCommandLineTest, EmptyPath)
{
    vector<fs::path> directories;
    fs::path logfile;
    string message;
    bool result = HandleCommandLine(0, 0, directories, logfile, message);
    EXPECT_FALSE(result);
}

TEST(HandleCommandLineTest, OneArg)
{
    vector<fs::path> directories;
    fs::path logfile;
    string message;
    vector<char*> args;
    args.push_back((char*)"--directory");
    args.push_back((char*)"c:\temp");
    bool result = HandleCommandLine(2, &args[0], directories, logfile, message);
    EXPECT_FALSE(result);
}
