#include "CommandLine-mock.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>

using namespace std;
using ::testing::AtLeast;

namespace fs = boost::filesystem;

TEST(CommandLineTest, EmptyPath)
{
    MockCommandLine mockCmdLine;
    EXPECT_CALL(mockCmdLine, ParseCommandLine()).Times(AtLeast(1));

    EXPECT_FALSE(mockCmdLine.ParseCommandLine());
}
