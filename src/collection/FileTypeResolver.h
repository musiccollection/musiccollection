/*****************************************************************************
 * Copyright 2014 Bart van der Velden <bart@muckingabout.eu>                 *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/

#ifndef MUSICCOLLECTION_COLLECTION_FILETYPERESOLVER_H_INCLUDED
#define MUSICCOLLECTION_COLLECTION_FILETYPERESOLVER_H_INCLUDED

#include <fileref.h>

namespace mc
{
    namespace collection
    {
        //! Our FileTypeResolver for TagLib
        /*! This resolver will look at a file and return the proper TagLib::File as a result.
         */
        class FileTypeResolver : public TagLib::FileRef::FileTypeResolver
        {
        public:
            //! The TagLib virtual function to override
            /*! createFile creates the derived File type, e.g. TagLib::MPEG::File, based on
             * fileName's extension and/or content.
             * \param fileName the file to examine
             * \param readAudioProperties
             * \param audioPropertiesStyle determines how much to read from the file, this strongly
             * determines the time needed
             */
            virtual TagLib::File* createFile(
                TagLib::FileName fileName,
                bool readAudioProperties = true,
                TagLib::AudioProperties::ReadStyle audioPropertiesStyle =
                    TagLib::AudioProperties::Average) const;
        public:
            virtual ~FileTypeResolver()
            {
            }
        };
    } // namespace collection
} // namespace mc

#endif // MUSICCOLLECTION_COLLECTION_FILETYPERESOLVER_H_INCLUDED
