/*****************************************************************************
 * Copyright 2014 Bart van der Velden <bart@muckingabout.eu>                 *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/

#include "Album.h"

using namespace std;
using namespace mc::collection;

//! Construct an album
Album::Album(const QString& albumName, const QString& albumArtist)
    : m_albumName(albumName),
      m_albumArtist(albumArtist)
{
}

//! Add a track to the album
void Album::AddTrack(Track* track)
{
    m_tracks.append(track);
}

//! Set the album name
void Album::SetAlbumName(const QString& albumName)
{
    m_albumName = albumName;
}

//! Retrieve the album name
QString Album::AlbumName() const
{
    return m_albumName;
}

//! Set the album artist name
void Album::SetAlbumArtist(const QString& albumArtist)
{
    m_albumArtist = albumArtist;
}

//! Retrieve the album artist name
QString Album::AlbumArtist() const
{
    return m_albumArtist;
}

//! Give access to all the tracks that form this album
const QList<Track*>& Album::Tracks() const
{
    return m_tracks;
}
