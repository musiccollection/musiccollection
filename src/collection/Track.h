/*****************************************************************************
 * Copyright 2014 Bart van der Velden <bart@muckingabout.eu>                 *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/

#ifndef MUSICCOLLECTION_COLLECTION_TRACK_H_INCLUDED
#define MUSICCOLLECTION_COLLECTION_TRACK_H_INCLUDED

#include <QString>

namespace mc
{
    namespace collection
    {
        // Forward declaration
        class Directory;

        //! A track as read from a file system.
        /*! This class is used to extract all possible data from an audio file.
         */
        class Track
        {
        public:
            //! Construct a Track.
            /*! A Track is constructed from a path and a pointer to the Directory it is in.
             */
            Track(const QString& path, const Directory* dir);

            //! The absolute path to the file.
            /*! Because of symbolic links the path could be outside the original scanning
             * directories.
             */
            QString AbsolutePath() const;

            //! The relative path at the point of scanning.
            QString RelativePath() const;

        private:
            QString m_path;
            QString m_relativePath;
            const Directory* m_directory;

        private:
            //! The default constructor is not implemented.
            Track();
            Q_DISABLE_COPY(Track)
        };

    } // namespace collection
} // namespace ms

#endif // MUSICCOLLECTION_COLLECTION_TRACK_H_INCLUDED
