/*****************************************************************************
 * Copyright 2014 Bart van der Velden <bart@muckingabout.eu>                 *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/

#ifndef MUSICCOLLECTION_COLLECTION_DIRECTORY_H_INCLUDED
#define MUSICCOLLECTION_COLLECTION_DIRECTORY_H_INCLUDED

#include <QString>
#include <QList>

namespace mc
{
    namespace collection
    {
        // Forward declaration
        class Track;

        //! A Directory and its contents as scanned from the file system.
        class Directory
        {
        public:
            //! Construct a directory from its path.
            Directory(const QString& path);

            //! Add a track to the directory.
            void AddTrack(Track* track);

            //! Give access to the tracks in this directory.
            const QList<Track*> Tracks() const;
        private:
            QString m_path;
            QList<Track*> m_tracks;
        private:
            //! The default constructor is not implemented.
            Directory();
            Q_DISABLE_COPY(Directory)
        };

    } // namespace collection
} // namespace ms

#endif // MUSICCOLLECTION_COLLECTION_DIRECTORY_H_INCLUDED


