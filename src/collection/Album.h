/*****************************************************************************
 * Copyright 2014 Bart van der Velden <bart@muckingabout.eu>                 *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/

#ifndef MUSICCOLLECTION_COLLECTION_ALBUM_H_INCLUDED
#define MUSICCOLLECTION_COLLECTION_ALBUM_H_INCLUDED

#include <QString>
#include <QList>

namespace mc
{
    namespace collection
    {
        // Forward declarations
        class Directory;
        class Track;

        //! An album as assembled from read tracks that have the same album name and album artist
        /*! This class is used to collect all album data that was extracted from the tracks that
         * form the album
         */
        class Album
        {
        public:
            //! Construct an album
            Album(const QString& albumName, const QString& albumArtist);

            //! Add a track to the album
            void AddTrack(Track* track);

            //! Set the album name
            void SetAlbumName(const QString& albumName);

            //! Retrieve the album name
            QString AlbumName() const;

            //! Set the album artist name
            void SetAlbumArtist(const QString& albumArtist);

            //! Retrieve the album artist name
            QString AlbumArtist() const;

            //! Give access to all the tracks that form this album
            const QList<Track*>& Tracks() const;
        private:
            QString m_albumName;
            QString m_albumArtist;
            QList<Track*> m_tracks;
        private:
            //! The default constructor is not implemented
            Album();
            Q_DISABLE_COPY(Album)
        };

    } // namespace collection
} // namespace ms

#endif // MUSICCOLLECTION_COLLECTION_ALBUM_H_INCLUDED


