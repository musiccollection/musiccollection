#include <gtest/gtest.h>
#include "../FileTypeResolver.h"
#include <QString>
#include <QDir>
#include "testconfig.h"

#include <mpegfile.h>
#include <oggfile.h>
#include <vorbisfile.h>

using namespace std;
using namespace mc::collection;
using namespace TagLib;

class TestFileName
{
public:
    //
    TestFileName(const QString& fileName) : m_fileName(fileName)
    {
    }

    TagLib::FileName TagLibFileName() const
    {
#ifdef WIN32
        return reinterpret_cast<const wchar_t*> (m_fileName.constData());
#else
        m_byteArray = QFile::encodeName(m_fileName);
        return m_byteArray.constData();
#endif
    }

private:
    QString m_fileName;
#ifndef WIN32
    mutable QByteArray m_byteArray;
#endif
};

QString datapath(const QString& relPath)
{
    return QDir::toNativeSeparators(QString(MUSICCOLLECTION_TEST_DIR) + '/' + relPath );
}

TagLib::FileName QStringToFileName(const QString& path)
{
#ifdef WIN32
    return reinterpret_cast<const wchar_t*> (path.constData());
#else
    return QFile::encodeName(path).constData();
#endif
}

TEST(FileTypeResolverTest, EmptyPath)
{
    FileTypeResolver ftr;
    TestFileName fileName("");
    File* file = ftr.createFile(fileName.TagLibFileName());
    EXPECT_EQ(0, file);
}

TEST(FileTypeResolverTest, FileDoesNotExist)
{
    FileTypeResolver ftr;
    TestFileName fileName("ThisDoesNotExist");
    File* file = ftr.createFile(fileName.TagLibFileName());
    EXPECT_EQ(0, file);
}

TEST(FileTypeResolverTest, FileDoesNotExistMp3Ext)
{
    FileTypeResolver ftr;
    TestFileName fileName("ThisDoesNotExist.mp3");
    File* file = ftr.createFile(fileName.TagLibFileName());
    ASSERT_TRUE(file != 0);
    ASSERT_TRUE(dynamic_cast<MPEG::File*>(file) != 0);
    ASSERT_FALSE(file->isOpen());
}

TEST(FileTypeResolverTest, OggFileExists)
{
    FileTypeResolver ftr;
    TestFileName fileName(datapath("test.ogg"));
    File* file = ftr.createFile(fileName.TagLibFileName());
    ASSERT_TRUE(file != 0);
    ASSERT_TRUE(dynamic_cast<Ogg::Vorbis::File*>(file) != 0);
    ASSERT_TRUE(file->isOpen());
}

TEST(FileTypeResolverTest, JapaneseOggFile)
{
    FileTypeResolver ftr;
    TestFileName fileName(datapath("テスト.ogg"));
    File* file = ftr.createFile(fileName.TagLibFileName());
    ASSERT_TRUE(file != 0);
    ASSERT_TRUE(dynamic_cast<Ogg::Vorbis::File*>(file) != 0);
    ASSERT_TRUE(file->isOpen());
}

TEST(FileTypeResolverTest, ArabicOggFile)
{
    FileTypeResolver ftr;
    TestFileName fileName(datapath("اختبار.ogg"));
    File* file = ftr.createFile(fileName.TagLibFileName());
    ASSERT_TRUE(file != 0);
    ASSERT_TRUE(dynamic_cast<Ogg::Vorbis::File*>(file) != 0);
    ASSERT_TRUE(file->isOpen());
}

TEST(FileTypeResolverTest, RussianOggFile)
{
    FileTypeResolver ftr;
    TestFileName fileName(datapath("тест.ogg"));
    File* file = ftr.createFile(fileName.TagLibFileName());
    ASSERT_TRUE(file != 0);
    ASSERT_TRUE(dynamic_cast<Ogg::Vorbis::File*>(file) != 0);
    ASSERT_TRUE(file->isOpen());
}
