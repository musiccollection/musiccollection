/*****************************************************************************
 * Copyright 2014 Bart van der Velden <bart@muckingabout.eu>                 *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/

/* Credits:
 * The content of this file is based on the createFile implementation in Amarok combined with an
 * example on Qt5 mime types provided by Jeff Tranter at
 * http://www.ics.com/blog/whats-new-qt-5-qmimedatabase-and-qmimetype
 */

#include "FileTypeResolver.h"

// Qt includes
#include <QFile>
#include <QMimeType>
#include <QMimeDatabase>

// TagLib includes
#include <mpegfile.h>
#include <oggfile.h>
#include <vorbisfile.h>
#include <flacfile.h>
#include <oggflacfile.h>

using namespace std;
using namespace mc::collection;
using namespace TagLib;

QString FileNameToQString(const TagLib::FileName& fileName)
{
#ifdef WIN32
    return QString::fromStdWString(fileName.wstr());
#else
    return QFile::decodeName(fileName);
#endif
}

/* createFile creates the derived File type, e.g. TagLib::MPEG::File, based on
 * fileName's extension and/or content.
 */
File* FileTypeResolver::createFile(
    FileName fileName,
    bool readAudioProperties /* = true */,
    AudioProperties::ReadStyle audioPropertiesStyle /* = AudioProperties::Average */) const
{
    File* result = 0;
    QMimeDatabase mimeDatabase;

    QString fn = FileNameToQString(fileName);
    QMimeType mimeType = mimeDatabase.mimeTypeForFile(fn);
    QString mtn = mimeType.name();

    if ((mimeType.name() == QString("audio/mpeg")) ||
        (mimeType.name() == QString("audio/x-mpegurl")))
    {
        result = new MPEG::File(fileName, readAudioProperties, audioPropertiesStyle);
    }
    else if ((mimeType.name() == QString("audio/vorbis")) ||
             (mimeType.name() == QString("audio/x-vorbis+ogg")) ||
             (mimeType.name() == QString("audio/ogg")))
    {
        result = new Ogg::Vorbis::File(fileName, readAudioProperties, audioPropertiesStyle);
    }
    else if ((mimeType.name() == QString("audio/x-flac+ogg")))
    {
        result = new Ogg::FLAC::File(fileName, readAudioProperties, audioPropertiesStyle);
    }
    else if ((mimeType.name() == QString("audio/x-flac")))
    {
        result = new FLAC::File(fileName, readAudioProperties, audioPropertiesStyle);
    }

    return result;
}
