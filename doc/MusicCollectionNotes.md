# MusicCollection #

<!-- Use a html style image to enable aligning to the right -->
<img style="float:right" src="./images/icon.png" />

This document contains design decisions, notes, To-do items and anything else that should not be forgotten for the MusicCollection project.

## The products ##

It is my intention to create the following products:

1. A program that scans a set of given directories for music files,  collects all the metadata from the files and stores it in a database. The program monitors the directories for changes and keeps the database up-to-date with the music files. The program exposes an interface for other applications to query the information about music files.
2. A PC application that acts as a GUI to the program from 1. This application allows to show lists and query the database and show the results.
3. A web site that allows to do about the same as the application from 2.
4. A mobile (Android) application that does the same as the products from 2. and 3.

The goal of these products is to learn about PC, web and mobile development.

## Requirements ##

This section is going to contain the requirements for all the projects. Each requirement will get a unique number that is not going to change. When a requirement is deleted, its number will not be reused for a new requirement.

### General requirements ###

### Requirements for the development environment ###
 
### Requirements for the service ###

### Requirements for the PC application ###

### Requirements for the web site ###

### Requirements for the mobile application ###

## Development environment and tools ##

The MusicCollection development is performed using a number of tools and web sites.

- The MusicCollection [git repository](https://gitorious.org/musiccollection)
- Bugs and change requests are administered in [Redmine](https://redmine.muckingabout.eu/projects/music_collection)
- Continuous integration is done at my [Jenkins page](https://jenkins.muckingabout.eu)

I do my local development both on a Kubuntu 13.10 and two Windows 7 machines. On all machines I installed the following software:

- git
- CMake
- Qt Creator
- Boost
- TagLib

On Windows I use:

- Microsoft Visual Studio 2010  and Visual Studio 2012 next to Qt Creator
- TortoiseGit

On Kubuntu I use:

- Occasionally KDevelop and make from the command line

The file INSTALL in the root of the repository contains more details on libraries to use and how to install them.

## To-do list ##

The items on this list are to be evaluated and removed from the list when an action is defined. The [Redmine](https://redmine.muckingabout.eu/projects/music_collection) page contains the actions that need to be performed.
